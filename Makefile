JOBNAME=fascicule
SRCNAME=main
BUILDDIR=build
LATEXBACKEND?=pdflatex
LATEXOPT=-interaction=batchmode -file-line-error
#LATEXMKOPT=-jobname=$(JOBNAME) -auxdir=$(BUILDDIR) -outdir=$(BUILDDIR) -$(LATEXBACKEND)="$(LATEXBACKEND) -interaction=batchmode -file-line-error" -use-make
LATEXMK?=latexmk
#LATEXMKOPT=-pdf -jobname=$(JOBNAME) -auxdir=$(BUILDDIR) -outdir=$(BUILDDIR) -$(LATEXBACKEND)="$(LATEXBACKEND) $(LATEXOPT)" -use-make
LATEXMKOPT=-pdf -jobname=$(JOBNAME) -$(LATEXBACKEND)="$(LATEXBACKEND) $(LATEXOPT)" -use-make


#COLORLOG:=grep --color=always --line-buffered -vP "^\s*((\[|\]|\(|\))\s*)+$$" | GREP_COLOR="01;31" grep --color=always --line-buffered -P "(^[^:]*:\d+: )|(^l\.\d+ )|(^! LaTeX Error: )|$$" | GREP_COLOR="00;33" grep --color=always --line-buffered -P "(LaTeX Warning(:|))|(Package [^\s]+ Warning(:|))|$$" | GREP_COLOR="00;32" grep --color=always --line-buffered -P "^Output written on .*\.pdf \(.*\)\.$$|$$"

.PHONY: $(JOBNAME).pdf all clean

all: $(JOBNAME).pdf


$(JOBNAME).pdf: $(SRCNAME).tex
	#@mkdir -p $(BUILDDIR)
	@$(LATEXMK) $(LATEXMKOPT) $<
	#@cp $(BUILDDIR)/$(JOBNAME).pdf $(JOBNAME).pdf
	#@echo -ne "\033[0;32m"
	#@echo "┌────────────────┐"
	#@echo "│ Output summary │"
	#@echo "└────────────────┘"
	#@echo -ne "\033[m"
	#@cat $(BUILDDIR)/$(JOBNAME).log | $(COLORLOG) | grep -A5 --color=never -P "\033"

clean:
	@latexmk -C -quiet $(LATEXMKOPT)
	#@rmdir $(BUILDDIR)
	@rm -r $(BUILDDIR)
	#@rm  $(JOBNAME).pdf

visu:
	@zathura $(BUILDDIR)/$(JOBNAME).pdf &


my_test:
ifdef todo
		@latexmk -pdf -auxdir='build_'$(todo) -outdir='build_'$(todo) $(todo).tex 
else
		echo 'hello'
endif

